FROM node:16.13.1-slim

RUN mkdir -p /usr/local/k8s-test-api/node_modules && chown -R node:node /usr/local/k8s-test-api

WORKDIR /usr/local/k8s-test-api

COPY package.json yarn.lock tsconfig*.json nest-cli.json ./

USER node

RUN yarn

COPY --chown=node:node src/ src/

RUN yarn run build

CMD ["node", "dist/main"]
