import { Injectable } from '@nestjs/common';
import 'dotenv/config';
import { hostname, networkInterfaces } from 'os';

@Injectable()
export class AppService {
  getHello(): Record<string, unknown> {
    return {
      message: 'Hello World!',
      version: process.env.VERSION,
      pid: process.pid,
      hostname: hostname(),
      interfaces: networkInterfaces(),
    };
  }
}
